package asiusb.server;

import asiusb.client.GreetingService;
import asiusb.shared.FieldVerifier;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;

/**
 * The server side implementation of the RPC service.
 */
@SuppressWarnings("serial")
public class GreetingServiceImpl extends RemoteServiceServlet implements
    GreetingService {

  public String greetServer(String input) throws IllegalArgumentException {
    // Verify that the input is valid.
    if (!FieldVerifier.isValidName(input)) {
      // If the input is not valid, throw an IllegalArgumentException back to
      // the client.
      throw new IllegalArgumentException(
          "Name must be at least 4 characters long");
    }

    String serverInfo = getServletContext().getServerInfo();
    String userAgent = getThreadLocalRequest().getHeader("User-Agent");

    // Escape data from the client to avoid cross-site script vulnerabilities.
    input = escapeHtml(input);
    userAgent = escapeHtml(userAgent);

    //Charger ici la base de donnée
    String[] tableauDesIdentifiants = new String[100];

    tableauDesIdentifiants[0]="pierre";
    tableauDesIdentifiants[1]="zenan";
    tableauDesIdentifiants[2]="junzhe";
    int nbid = 3;
    
    for(int i=0;i<nbid;i++){
	if (tableauDesIdentifiants[i].equals(input)){
	    return "Dommage : " + input + " est déjà enregistré dans la base.<br><br> <b>Veuillez recommencer la procédure d'inscription avec un autre identifiant.</b>";
	}
    }

    return "Votre identifiant : " + input + " n'était pas déjà enregistré dans la base.<br><br> <b>Vous êtes désormais correctement inscrit au service.</b>";
    
  }

  /**
   * Escape an html string. Escaping data received from the client helps to
   * prevent cross-site script vulnerabilities.
   *
   * @param html the html string to escape
   * @return the escaped string
   */
  private String escapeHtml(String html) {
    if (html == null) {
      return null;
    }
    return html.replaceAll("&", "&amp;").replaceAll("<", "&lt;").replaceAll(
        ">", "&gt;");
  }
}
