# Agenda Social Intelligent #
Un emploi du temps social pour les étudiants ASI.

[http://casisbelli-dev.insa-rouen.fr/agendaSocialIntelligent/](http://casisbelli-dev.insa-rouen.fr/agendaSocialIntelligent/)



### Comment tester le projet ? ###


```
#!sh

mvn gwt:run
```
### Comment redéployer le projet ? ###


```
#!sh

mvn tomcat:redeploy 
```



### MOE équipe Asiusb ###

Pierre Besson

Junzhe Hao

Zenan Xu